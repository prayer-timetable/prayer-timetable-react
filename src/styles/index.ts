/* eslint-disable babel/semi */
/* eslint-disable babel/no-unused-expressions */
/* eslint-disable no-console */
/* eslint-disable arrow-body-style */
/* eslint-disable arrow-parens */
/* eslint-disable no-confusing-arrow */
/* eslint-disable implicit-arrow-linebreak */
/* eslint-disable indent */
import styled, { createGlobalStyle, css } from 'styled-components'
// @ts-ignore
import bg from '../config/bg.png'

interface PrayerProps {
  prayer: any
  next: any
}
// interface CountdownProps {
//   percentage: any;
// }

import './normalize.css'
// import './fonts/open-sans.css'
import './fonts/cairo.css' // Cairo:200,300,400,600,700,900&amp;subset=arabic,latin-ext

const renderExtra = (extra: any) => {
  // console.log(extra)
  let result = ''
  for (const key in extra) {
    if (extra.hasOwnProperty(key)) {
      // console.log(`${key}: ${extra[key]};`)
      result = result + `${key}: ${extra[key]};`
    }
  }
  return result
}

// const bg = require('../config/bg.png')

import * as theme from '../config/theme.json'

const animations = {
  animationCurveFastOutSlowIn: 'cubic-bezier(0.4, 0, 0.2, 1)',
  animationCurveLinearOutSlowIn: 'cubic-bezier(0, 0, 0.2, 1)',
  animationCurveFastOutLinearIn: 'cubic-bezier(0.4, 0, 1, 1)',
  animationCurveDefault: 'cubic-bezier(0.4, 0, 0.2, 1)',
  animationSpeedDefault: '250ms',
  animationSpeedFast: '200ms',
  animationSpeedSlow: '300ms',
}

const componentStyle = (text: any, background: any, border: any, extra: any) => {
  return `
      color: ${text.color};
      font-weight: ${text.weight};
      font-size: ${text.size};
      font-family: ${text.family};
      // background-color: ${background.color};
      background-image: ${background.type}-gradient(${background.color},
        ${background.color2});
      ${background.image ? `background-image: url('${bg}')` : ''};
      border-width: ${border.width};
      border-style: ${border.style};
      border-color: ${border.color};
      ${extra ? renderExtra(extra) : ''}
    `
}

const defaultComponentStyle = theme.main || {
  text: {
    color: 'rgba(30,30,30,1)',
    color2: 'rgba(30,30,30,1)',
    size: '24px',
    size2: '24px',
    weight: 400,
    weight2: 400,
    family: 'Cairo',
  },
  background: {
    color: 'rgba(30,30,30,0)',
    color2: 'rgba(30,30,30,0)',
    type: 'linear',
  },
  border: {
    width: '0',
    style: 'solid',
    color: 'rgba(100,175,100,1)',
  },
  extra: {
    textDecoration: 'none',
  },
}
/* eslint-disable object-curly-newline */
const {
  main = defaultComponentStyle,
  header = defaultComponentStyle,
  clock = defaultComponentStyle,
  prayers = defaultComponentStyle,
  prayer = defaultComponentStyle,
  countdown = defaultComponentStyle,
  message = defaultComponentStyle,
  footer = defaultComponentStyle,
  overlay = defaultComponentStyle,
} = theme
// const { header, main, clock, prayers, prayer, countdown, message, footer } = theme

const flex = (direction = 'row') => {
  return `
      display: flex;
      box-sizing: border-box;
      flex-basis: auto; //0; 200px;
      justify-content: space-between;
      align-items: center;
      // width: 100%;
      // width: calc(100% - 2rem);
      // height: 100%;
      flex-direction: ${direction};
    `
}

export default {
  GlobalStyle: createGlobalStyle`
    * {
      margin: 0;
      padding: 0;
      box-sizing: border-box;
    }
    html, body, #root, #index {
      height: 100%;
    }
    strong {
      font-weight: 600;
      color: #000;
    }
    #root, #index {
      ${componentStyle(main.text, main.background, main.border, main.extra)} // style
    }
    body {
      background: url('${bg}') ${main.background.color};
      color: ${main.text.color};
      font: ${main.text.family}, 'Geneva', 'sans-serif';
    }

    h1, h2, h3, h4, h5, h6{
      font-family: 'Cairo', 'Oswald', 'Geneva', 'sans-serif';
      font-weight: 700;
      margin: 0;
      padding: 0;
      line-height: 1.25;
    }
    p, ul, li {
      margin: 0;
      padding: 0;
      line-height: 1.2;
    }
  `,
  animations,
  // color,
  // font,

  AppStyle: styled.section`
    ${flex('column')}
    width: 100%;
    height: 100%;
  `,

  header: styled.div`
    ${flex()} width: 100%;
    font-size: ${header.text.size};
    height: 4%;
    ${componentStyle(header.text, header.background, header.border, header.extra)} // other
    padding: 0.25rem 2rem;
    div {
      ${flex()}
      flex: 1;

      :last-child {
        justify-content: flex-end;
        text-align: right;
      }
    }
    & .logo {
      justify-content: center;
      width: 10%;
      height: 3.6rem;
      img {
        height: 3.6rem;
        padding-right: 0.25rem;
      }
    }
  `,

  clock: styled.section`
    ${flex()}
    justify-content: center;
    width: 100%;
    height: 15%;
    ${componentStyle(clock.text, clock.background, clock.border, clock.extra)} // style

    & .body {
      ${flex('column')} .timeRow {
        font-size: ${clock.text.size};
        font-weight: ${clock.text.weight};
      }
      .dateRow {
        font-size: ${clock.text.size2};
        font-weight: ${clock.text.weight2};
        text-align: center;
      }
    }
  `,

  prayers: styled.section`
    ${flex('column')} height: 50%;
    width: 100%;
    ${componentStyle(prayers.text, prayers.background, prayers.border, prayers.extra)} // style


    .prayerHeader {
      ${flex()} width: 100%;
      padding: 0.25rem 2rem;
      // flex: .2;
      height: 100%;
      border-width: ${prayers.border.width};
      border-style: ${prayers.border.style};
      border-color: ${prayers.border.color};
    }
  `,

  prayer:
    styled.div <
    PrayerProps >
    `
    ${flex()} width: 100%;
    text-transform: uppercase;
    height: 100%;
    padding: 0.125rem 2rem;
    ${componentStyle(prayer.text, prayer.background, prayer.border, prayer.extra)} // style
    background: ${prayer.background.color};
    :last-child {
      border: none;
    }
    // padding: 1rem;
    div {
      width: 100%;
      :nth-child(2n) {
        text-align: center;
      }
      :first-child {
        text-align: left;
      }
      :last-child {
        text-align: right;
      }
    }
    & .pending {
      :before {
        content: '⇒ ';
      }
    }
    ${props =>
      props.next
        ? `background: ${prayer.background.color2};color: ${prayer.text.color2};font-weight: ${prayer.text.weight2};`
        : ''};
  `,

  countdown: styled.div`
    ${flex()} // flex
    width: 100%;
    ${componentStyle(
      countdown.text,
      countdown.background,
      countdown.border,
      countdown.extra,
    )} // style
    justify-content: center;
    height: 9%;
    div {
      padding: 0 0.5rem 1%;
      z-index: 1;
      // margin-bottom: 1%;
    }
    position: relative;
  `,

  message: styled.div`
    ${flex('column')} justify-content: space-around;
    height: 18%;
    padding: 0.1rem 2rem;
    width: 100%;
    ${componentStyle(message.text, message.background, message.border, message.extra)} // style


    div {
      margin: 0;
    }
    .en {
      font-size: ${message.text.size};
      font-weight: ${message.text.weight};
      text-align: left;
    }
    .ar {
      font-size: ${message.text.size2};
      font-weight: ${message.text.weight2};
      text-align: right;
    }
  `,

  footer: styled.div`
    ${flex()} width: 100%;
    height: 4%;
    ${componentStyle(footer.text, footer.background, footer.border, footer.extra)} // style
    padding: 0.25rem 2rem;
    div {
      ${flex()}
      flex: 1;
      justify-content: center;
      svg {
        padding-right: 0.75rem;
      }
      :first-child {
        justify-content: flex-start;
      }
      :last-child {
        font-size: ${footer.text.size2};
        justify-content: flex-end;
      }
    }
    div.info {
      flex-direction: column;
      align-items: flex-end;
    }
    .wifi {
      svg {
        // width: 0.75rem;
        height: 1.6rem;
        .on {
          fill: ${footer.text.color};
        }
        .off {
          fill: ${footer.text.color2};
        }
      }
    }
  `,

  overlay: styled.div`
    ${flex('column')} justify-content: space-between;
    z-index: 999;

    color: ${overlay.text.color};
    background: ${overlay.background.color};

    flex: 0.175;
    position: fixed;
    padding: 2rem 1rem;
    width: 100%;
    height: 100%;
    font-size: 125%;
    div {
      margin: 0;
      text-align: center;
    }
    & .title {
      font-size: 75%;
    }
    h1 {
      color: ${overlay.text.color2};
      font-size: 150%;
      padding-bottom: 2rem;
    }
    & .logo {
      justify-content: center;
      width: 10%;
      height: 10rem;
      img {
        height: 10rem;
        padding-right: 0.25rem;
      }
    }
  `,

  main: css`
    color: ${main.text.color};
    margin-left: 1rem;
    display: inline-block;
    // float: right;
  `,

  light: css`
    color: ${main.text.color2};
  `,

  uppercase: css`
    text-transform: lowercase;
    font-size: 90%;
    font-weight: 500;
  `,

  smallText: css`
    font-size: 85%;
    a {
      padding: 0.25rem 0;
    }
  `,
}
// console.log(colors)
