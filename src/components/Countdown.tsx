import * as React from 'react'

import { connect } from 'react-redux'
import { format } from 'date-fns'

import styles from '../styles'

import * as theme from '../config/theme.json'

const { countdown: countdownTheme } = theme

// import { down, up, circle, circleBg } from '../helpers/svg'
import { appendZero, convertToDuration } from '../helpers/func'
import { State } from '../store/reducers/initialState'
type StateProps = Pick<State, 'prayers' | 'day'>

const CountdownStyle = styles.countdown

const mapStateToProps = (state: StateProps) => ({ prayers: state.prayers, day: state.day })

const ConnectedCountdown = ({ prayers }: StateProps) => (
  <CountdownStyle>
    <div>{prayers.countDown.name}</div>
    {/* {console.log(convertToDuration(prayers.countDown.duration))} */}
    {/* {console.log(prayers.countDown)} */}
    <div className="countdown">{convertToDuration(prayers.countDown.duration)}</div>
    {/* <div className="countdown">
      {`${prayers.countDown.duration.hours()}:${appendZero(prayers.countDown.duration.minutes())}:${appendZero(
        prayers.countDown.duration.seconds(),
      )}`}
    </div> */}
    <div
      style={{
        position: 'absolute',
        bottom: '0',
        left: '0',
        content: '',
        width: `${prayers.percentage}%`,
        height: '2%',
        background: `${countdownTheme.border.color}`,
      }}
    />
  </CountdownStyle>
)
const Countdown = connect(mapStateToProps)(ConnectedCountdown)
export default Countdown
