import { combineReducers } from 'redux'
import timetableReducer from './timetableReducer'
import prayersReducer from './prayersReducer'
import settingsReducer from './settingsReducer'
import dayReducer from './dayReducer'
import updateReducer from './updateReducer'

const rootReducer = combineReducers({
  timetable: timetableReducer,
  prayers: prayersReducer,
  settings: settingsReducer,
  day: dayReducer,
  update: updateReducer,
})

export default rootReducer
