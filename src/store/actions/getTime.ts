import { dayCalc } from 'prayer-timetable-lib'
// import { dayCalc } from '../../helpers/test_calc'
// dayCalc = (offsetDay = 0, offSetHour = 0, hijrioffset = 0, city = 'Europe/Dublin')
// return { now, month, date, start, end, hijri, dstAdjust }
// @ts-ignore
import { setDay, setHours, setMinutes, isWithinInterval, addHours, startOfDay } from 'date-fns'
import store from '../index'
import { GET_TIME } from '../constants/actionTypes'
import { hijriConvert } from '../../helpers/func'
// console.log('STORE', store.getState())

const getTime = () => {
  const { now, hijri } = dayCalc(0, 0, {
    hijrioffset: store.getState().settings.hijrioffset,
  })

  // OVERLAY
  const jummuahTime = setDay(
    setHours(
      setMinutes(new Date(), store.getState().settings.jummuahtime[1]),
      store.getState().settings.jummuahtime[0],
    ),
    5,
  )
  const taraweehTime = setHours(
    setMinutes(new Date(), store.getState().settings.taraweehtime[1]),
    store.getState().settings.taraweehtime[0],
  ) // .iMonth(8),

  let overlayActive
  let overlayTitle

  if (isWithinInterval(new Date(), { start: jummuahTime, end: addHours(jummuahTime, 1) })) {
    overlayActive = true
    overlayTitle = 'Jummuah Prayer'
  } else if (
    // taraweeh in ramadan first part
    hijriConvert(new Date()).hm === 9 &&
    isWithinInterval(new Date(), { start: taraweehTime, end: addHours(taraweehTime, 2) })
  ) {
    overlayActive = true
    overlayTitle = 'Taraweeh Prayer'
  } else if (
    // taraweeh in ramadan second part
    hijriConvert(new Date()).hm === 9 &&
    isWithinInterval(new Date(), { start: startOfDay(new Date()), end: addHours(startOfDay(new Date()), 1) })
  ) {
    overlayActive = true
    overlayTitle = 'Taraweeh Prayer'
  } else {
    overlayActive = false
    overlayTitle = ' ... '
  }

  return store.dispatch({
    type: GET_TIME,
    now,
    hijri,
    overlayActive,
    overlayTitle,
    // prayers: store.getState().prayers,
  })
}

export default getTime
